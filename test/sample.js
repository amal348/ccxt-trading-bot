const ccxt = require('ccxt');

const tryToCreateOrder = async function (exchange, symbol, type, side, amount, price, params) {

    try {

        const order = await exchange.createOrder (symbol, type, side, amount, price, params)
        return order

    } catch (e) {

        console.log (e.constructor.name, e.message)

        if (e instanceof ccxt.NetworkError) {

            // retry on networking errors
            return false

        } else {

            throw e // break on all other exceptions
        }
    }
}

const exchange = new ccxt.bitmart ({
    'apiKey': '745e0da655178fba6520954887962a42c1f3a72c', // edit here
    'secret': 'ab78b38e0a79391d31f922bd9f497aded2d26f074cf8572aa8cc1079a33796e9',  // edit here
    'uid':'amalk',
})

//
// make a classic bytetrade account - one that is linked to an email or phone number
// then click on your username in the top right and then export
// you will get a file like this:
//
// future garage icon motion panda garage motion task science head garage notable
// ebcefff7de475ffe15e864ca3e3e410edf7e94fffd1f9af34edf9434e2bfff1b
// classic123
//
// the second line is your secret and the third line is your apiKey
//

const symbol = 'BTC/USDT' // edit here
const type = 'limit'     // edit here
const side = 'buy'        // edit here
const amount = 10         // edit here
const price = 1           // edit here
const params = {}         // edit here

;(async () => {
    let order = false
    while (true) {
        order = await tryToCreateOrder (exchange, symbol, type, side, amount, price, params)
        if (order !== false) {
            break
        }
    }
    console.log (order)
}) ()




//pkill node                           ---Done Ok

//npx kill-port 3000







































// // this example is oversimplified and doesn't show all the code that is
// // required to handle the errors and exchange metadata properly
// // it shows just the concept of placing a market buy order
// const ccxt = require('ccxt');

// const exchange = new ccxt.cex ({
//     'apiKey': 'bc2b52cfc09755abfddabe44fd6d65a87419afbc',
//     'secret': '16450cecd6ef631e7704565587a2a7052c5e19a1131c1ceed1b9149afb93f46a',
//     'enableRateLimit': true,
//     // 'options': {
//     //     'createMarketBuyOrderRequiresPrice': true, // default
//     // },
// })

// ;(async () => {

//     // when `createMarketBuyOrderRequiresPrice` is true, we can pass the price
//     // so that the total cost of the order would be calculated inside the library
//     // by multiplying the amount over price (amount * price)

//     const symbol = 'BTC/USD'
//     const amount = 2 // BTC
//     const price = 9000 // USD
//     // cost = amount * price = 2 * 9000 = 18000 (USD)

//     // note that we don't use createMarketBuyOrder here, instead we use createOrder
//     // createMarketBuyOrder will omit the price and will not work when
//     // exchange.options['createMarketBuyOrderRequiresPrice'] = true
//     const order = await exchange.createOrder (symbol, 'market', 'buy', amount, price)

//     console.log (order)
// }) ()
