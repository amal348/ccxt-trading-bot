const ccxt = require('ccxt');

module.exports = {
//--------------------------------------------------FETCH BALANCE------------------------------------------------------------------------------------    
    fetchBalance: async (req,res) => {
        const {
            ExchangeId,
          } = req.body;
        const exchange = new ccxt[ExchangeId]({
            'apiKey': process.env.apiKey,
            'secret': process.env.secret,
            uid: process.env.uid,
            // 'password':process.env.password,
            // url: "https://api-cloud.bitmart.com"
        });
        
      
        try {
            
            const balance = await exchange.fetchBalance();
            console.log(balance);
            //console.log("BTC_balance: ",balance['total']['QNT']);
            res.json(balance);
                       
          
        } catch (err) {
            res.json({ message: err});
            console.log(err);
        }
    },
//-----------------------------------------------------------------FETCH MY TRADE-----------------------------------------------------------------------
    fetchMyTrade: async (req,res) => {
        const {
            // ExchangeId,
            market,
            // since,
            // limit,
            // params
          } = req.body;
        const exchange = new ccxt.bitmart({
            apiKey: process.env.apiKey,
            secret: process.env.secret,
            uid: process.env.uid,
            // url: "https://api-cloud.bitmart.com"
        });
        
      
        try {
            if (exchange.has['fetchMyTrades']) {
            const fetchMyTrades = await exchange.fetchMyTrades(market, since = undefined, limit = undefined, params = {});
            console.log(fetchMyTrades);
            //console.log("BTC_balance: ",balance['total']['QNT']);
            res.json(fetchMyTrades);
            }
                       
          
        } catch (err) {
            res.json({ message: err});
            console.log(err);
        }
    },

//-----------------------------------------------------------CREATE ORDER-----------------------------------------------------------------------------

    createOrder: async (req,res) => {
        const {
            // ExchangeId,
            symbol,
            amount,
            price,
            side
          } = req.body;
        
        const exchange = new ccxt.bitmart({
            apiKey: process.env.apiKey,
            secret: process.env.secret,
            uid: process.env.uid,
            // 'options': {
           //  'createMarketBuyOrderRequiresPrice': true, // default
          // },
             
        });
        // exchange.setSandboxMode (false);
           // when `createMarketBuyOrderRequiresPrice` is true, we can pass the price
          // so that the total cost of the order would be calculated inside the library
         // by multiplying the amount over price (amount * price)

        try {
            //  const symbol = 'BTC/USD'
            //  const amount = 2 // BTC
            //  const price = 9000 // USD
            // cost = amount * price = 2 * 9000 = 18000 (USD)

             // note that we don't use createMarketBuyOrder here, instead we use createOrder
            // createMarketBuyOrder will omit the price and will not work when
         // exchange.options['createMarketBuyOrderRequiresPrice'] = true
             const order = await exchange.createOrder (symbol, 'market', side, amount, price);
             console.log (order);                               //market/limit are type
             res.json(order);       
          
        } catch  (e) {
            // if the exception is thrown, it is "caught" and can be handled here
            // the handling reaction depends on the type of the exception
            // and on the purpose or business logic of your application
            if (e instanceof ccxt.NetworkError) {
                console.log (exchange.id, 'createOrder failed due to a network error:', e.message)
                res.json({
                    NetworkError:e.message,
                    });
            } else if (e instanceof ccxt.ExchangeError) {
                console.log (exchange.id, 'createOrder failed due to exchange error:', e.message)
                res.json({
                    Exchange_Error:e.message,
                    });
            } else {
                console.log (exchange.id, 'createOrder failed with:', e.message)
                // retry or whatever
                // ...
            }
        }
        
        
        // catch (err) {
        //     res.json({ message: err});
        //     console.log(err);
        // }
    },

//-----------------------------------------------------------------CREATE MARKET BUY ORDER-----------------------------------------------------------------------

        createMarketBuyOrder:  async (req,res) => {
        const {
            ExchangeId,
            symbol,
            amount,
            price
          } = req.body;
    
          const exchange = new ccxt.bitmart({
            apiKey: process.env.apiKey,
            secret: process.env.secret,
            uid: process.env.uid,
            enableRateLimit: true,
        'options': {
            'createMarketBuyOrderRequiresPrice': false, // switch off
        },
        
    })
    
    // or, to switch it off later, after the exchange instantiation, you can do
       exchange.options['createMarketBuyOrderRequiresPrice'] = false
    
    
    
        // when `createMarketBuyOrderRequiresPrice` is true, we can pass the price
        // so that the total cost of the order would be calculated inside the library
        // by multiplying the amount over price (amount * price)
    
        try {
            
            // const symbol; //'BTC/USD'
            // const amount ;// 2//BTC
            // const price ;// 9000//USD
            const cost = amount * price // ← instead of the amount cost goes ↓ here
            const order = await exchange.createMarketBuyOrder (symbol, cost);
            console.log (order);
            res.json(order);       
         
       } catch  (e) {
           if (e instanceof ccxt.NetworkError) {
               console.log (exchange.id, 'createMarketBuyOrder failed due to a network error:', e.message)
               res.json({
                NetworkError:e.message,
                });
               
           } else if (e instanceof ccxt.ExchangeError) {
               console.log (exchange.id, 'createMarketBuyOrder failed due to exchange error:', e.message)
               res.json({
                Exchange_Error:e.message,
                });
           } else {
               console.log (exchange.id, 'createMarketBuyOrder failed with:', e.message)
               res.json(e.message);
               
           }
       }
       
    
    },

   //----------------------------------------------------------CREATE MARKET SELL ORDER--------------------------------------------------------------------------
   createMarketSellOrder: async (req,res) => {
    const {
        // ExchangeId,
        symbol,
        amount,
      } = req.body;

      const exchange = new ccxt.bitmart({
        apiKey: process.env.apiKey,
        secret: process.env.secret,
        uid: process.env.uid,
        enableRateLimit: true,
    
    })


    try {
        // sell 1 BTC/USDT for market price, sell a bitcoin for dollars immediately
        const order = await exchange.createMarketSellOrder (symbol, amount);
        console.log (order);
        res.json(order);       
     
   } catch  (e) {
       if (e instanceof ccxt.NetworkError) {
           console.log (exchange.id, 'createMarketSellOrder failed due to a network error:', e.message)
           res.json({
            NetworkError:e.message,
            });
           
       } else if (e instanceof ccxt.ExchangeError) {
           console.log (exchange.id, 'createMarketSellOrder failed due to exchange error:', e.message)
           res.json({
            Exchange_Error:e.message,
            });
       } else {
           console.log (exchange.id, 'createMarketSellOrder failed with:', e.message)
           res.json(e.message);
           
       }
   }
   

},
 //------------------------------------------------------------ CREATE LIMIT BUY ORDER----------------------------------------------------------------------------
 createLimitBuyOrder: async (req,res) => {
    const {
        // ExchangeId,
        symbol,
        amount,
        price
      } = req.body;

      const exchange = new ccxt.bitmart({
        apiKey: process.env.apiKey,
        secret: process.env.secret,
        uid: process.env.uid,
        enableRateLimit: true,
    
    })

    try {
        // buy 1 BTC/USDT for $2500, you pay $2500 and receive ฿1 when the order is closed
        const order = await exchange.createLimitBuyOrder (symbol, amount, price);
        console.log (order);
        res.json(order);       
     
   } catch  (e) {
       if (e instanceof ccxt.NetworkError) {
           console.log (exchange.id, 'createLimitBuyOrder failed due to a network error:', e.message)
           res.json({
            NetworkError:e.message,
            });
           
       } else if (e instanceof ccxt.ExchangeError) {
           console.log (exchange.id, 'createLimitBuyOrder failed due to exchange error:', e.message)
           res.json({
            Exchange_Error:e.message,
            });
       } else {
           console.log (exchange.id, 'createLimitBuyOrder failed with:', e.message)
           res.json(e.message);
           
       }
   }
},
//--------------------------------------------------------CREATE LIMIT SELL ORDER--------------------------------------------------------------------------------

createLimitSellOrder: async (req,res) => {
    const {
        // ExchangeId,
        symbol,
        amount,
        price
      } = req.body;

      const exchange = new ccxt.bitmart({
        apiKey: process.env.apiKey,
        secret: process.env.secret,
        uid: process.env.uid,
        enableRateLimit: true,
    
    })

    try {
        const order = await exchange.createLimitSellOrder (symbol, amount, price); //Some exchanges allow you to specify optional parameters for your order. You can pass your optional parameters and override your query with an associative array using the params argument to your unified API call. All custom params are exchange-specific, of course, and aren’t interchangeable, do not expect those custom params for one exchange to work with another exchange.
        console.log (order);
        res.json(order);       
     
   } catch  (e) {
       if (e instanceof ccxt.NetworkError) {
           console.log (exchange.id, 'createLimitSellOrder failed due to a network error:', e.message)
           res.json({
            NetworkError:e.message,
            });
       } else if (e instanceof ccxt.ExchangeError) {
           console.log (exchange.id, 'createLimitSellOrder failed due to exchange error:', e.message)
           res.json({
            Exchange_Error:e.message,
            });
       } else {
           console.log (exchange.id, 'createLimitSellOrder failed with:', e.message)
           res.json(e.message);
           
       }
   }
},
//-----------------------------------------------------------CREATE ORDER WITH CLIENT ORDER ID-----------------------------------------------------------------------------

    createOrder_with_clientOrderId: async (req,res) => {
    const {
        // ExchangeId,
        symbol,
        amount,
        price,
        side,
        clientOrderId
      } = req.body;
    
    const exchange = new ccxt.bitmart({
        apiKey: process.env.apiKey,
        secret: process.env.secret,
        uid: process.env.uid,
         
    });

    try {
         //The user can specify a custom "clientOrderId" field can be set upon placing orders with the "params". 
         //Using the clientOrderId one can later "distinguish between own orders".
         const order = await exchange.createOrder (symbol, 'market', side, amount, price, {
            'clientOrderId': clientOrderId,
        });
         console.log (order);                             
         res.json(order);       
      
    } catch  (e) {
        if (e instanceof ccxt.NetworkError) {
            console.log (exchange.id, 'createOrder_with_clientOrderId failed due to a network error:', e.message)
            res.json({
                NetworkError:e.message,
                });
        } else if (e instanceof ccxt.ExchangeError) {
            console.log (exchange.id, 'createOrder_with_clientOrderId failed due to exchange error:', e.message)
            res.json({
                Exchange_Error:e.message,
                });
        } else {
            console.log (exchange.id, 'createOrder_with_clientOrderId failed with:', e.message)
            
        }
    }
    
},

//-----------------------------------------------------------CREATE ORDER WITH STOP LIMIT-----------------------------------------------------------------------------
createOrder_with_stopLimit: async (req,res) => {
    const {
        // ExchangeId,
        symbol,
        amount,
        price,
        side,
        stopPrice
      } = req.body;
    
    const exchange = new ccxt.bitmart({
        apiKey: process.env.apiKey,
        secret: process.env.secret,
        uid: process.env.uid,
         
    });

    try {
        // const symbol = 'ETH/BTC'
           const type = 'limit';// or 'market', other types aren't unified yet
        // const side = 'sell'
        // const amount = 123.45 // your amount
        // const price = 54.321 // your price
    // overrides
      const params = {
    'stopPrice': stopPrice, // your stop price
    'type': 'stopLimit',
}
         
         const order = await exchange.createOrder (symbol, type, side, amount, price, params);
         console.log (order);                             
         res.json(order);       
      
    } catch  (e) {
        if (e instanceof ccxt.NetworkError) {
            console.log (exchange.id, 'createOrder_with_stopLimit failed due to a network error:', e.message)
            res.json({
                NetworkError:e.message,
                });
        } else if (e instanceof ccxt.ExchangeError) {
            console.log (exchange.id, 'createOrder_with_stopLimit failed due to exchange error:', e.message)
            res.json({
                Exchange_Error:e.message,
                });
        } else {
            console.log (exchange.id, 'createOrder_with_stopLimit failed with:', e.message)
            
        }
    }
    
},
//-----------------------------------------------------------CANCEL ORDER-----------------------------------------------------------------------------

  
cancelOrder: async (req,res) => {
    const {
        // ExchangeId,
        symbol,
        orderId
      } = req.body;

      const exchange = new ccxt.bitmart({
        apiKey: process.env.apiKey,
        secret: process.env.secret,
        uid: process.env.uid,    
    })

    try {
        //To cancel an existing order pass the order id to cancelOrder (id, symbol, params) / cancel_order (id, symbol, params) method.
        // Note, that some exchanges require a second symbol parameter even to cancel a known order by id.
        const order = await exchange.cancelOrder (orderId,symbol); //The cancelOrder() is usually used on open orders only. 
        console.log (order);                                       //However, it may happen that your order gets executed (filled 
        res.json(order);                                           //and closed) before your cancel-request comes in, so a cancel-request might hit an already-closed order.                                                                 
   } catch  (e) {         
       //A cancel-request might also throw a NetworkError indicating that the order might or might not have been canceled successfully
       // and whether you need to retry or not. Consecutive calls to cancelOrder() may hit an already canceled order as well.                                           
       if (e instanceof ccxt.NetworkError) {
           console.log (exchange.id, 'cancelOrder failed due to a network error:', e.message)
           res.json({
            NetworkError:e.message,                                  //Cancels order. Throws an OrderNotFound exception if the order is already closed or cancelled.
            });
       } else if (e instanceof ccxt.ExchangeError) {
           console.log (exchange.id, 'cancelOrder failed due to exchange error:', e.message)
           res.json({
            Exchange_Error:e.message,
            });
       } else {
           console.log (exchange.id, 'cancelOrder failed with:', e.message)
           res.json(e.message);
           
       }
   }
},

//-----------------------------------------------------------FETCH OPEN ORDERS-----------------------------------------------------------------------------
   
fetchOpenOrders: async (req,res) => {      //fetches a list of open orders.
    const {
        // ExchangeId,
        symbol,
        since,
        limit,
        // params
      } = req.body;

      const exchange = new ccxt.bitmart({
        apiKey: process.env.apiKey,
        secret: process.env.secret,
        uid: process.env.uid,
        enableRateLimit: true,
    
    })

    try {
        if (exchange.has['fetchOpenOrders']);
        const order = await exchange.fetchOpenOrders (symbol , since, limit); //Some exchanges allow you to specify optional parameters for your order. You can pass your optional parameters and override your query with an associative array using the params argument to your unified API call. All custom params are exchange-specific, of course, and aren’t interchangeable, do not expect those custom params for one exchange to work with another exchange.
        console.log (order);
        res.json(order);       
     
   } catch  (e) {
       if (e instanceof ccxt.NetworkError) {
           console.log (exchange.id, 'fetchOpenOrders failed due to a network error:', e.message)
           res.json({
            NetworkError:e.message,
            });
       } else if (e instanceof ccxt.ExchangeError) {
           console.log (exchange.id, 'fetchOpenOrders failed due to exchange error:', e.message)
           res.json({
            Exchange_Error:e.message,
            });
       } else {
           console.log (exchange.id, 'fetchOpenOrders failed with:', e.message)
           res.json({Error: e.message});
           
       }
   }
},

//-----------------------------------------------------------CLOSED ORDERS-----------------------------------------------------------------------------
   
fetchClosedOrders: async (req,res) => {      //Fetches closed and cancelled orders placed by account.
    const {
        // ExchangeId,
        symbol,
        since,
        limit,
        // params
      } = req.body;

      const exchange = new ccxt.bitmart({
        apiKey: process.env.apiKey,
        secret: process.env.secret,
        uid: process.env.uid,
        enableRateLimit: true,
    
    })

    try {
        if (exchange.has['fetchOpenOrders']);
        const order = await exchange.fetchClosedOrders (symbol , since, limit); //Some exchanges allow you to specify optional parameters for your order. You can pass your optional parameters and override your query with an associative array using the params argument to your unified API call. All custom params are exchange-specific, of course, and aren’t interchangeable, do not expect those custom params for one exchange to work with another exchange.
        console.log (order);
        res.json(order);       
     
   } catch  (e) {
       if (e instanceof ccxt.NetworkError) {
           console.log (exchange.id, 'fetchClosedOrders failed due to a network error:', e.message)
           res.json({
            NetworkError:e.message,
            });
       } else if (e instanceof ccxt.ExchangeError) {
           console.log (exchange.id, 'fetchClosedOrders failed due to exchange error:', e.message)
           res.json({
            Exchange_Error:e.message,
            });
       } else {
           console.log (exchange.id, 'fetchClosedOrders failed with:', e.message)
           res.json({Error: e.message});
           
       }
   }
},

//-----------------------------------------------------------FETCH ORDER-----------------------------------------------------------------------------
   
fetchOrder: async (req,res) => {      //fetches a single order (open or closed) by order id
    const {
        // ExchangeId,
        id,
        symbol,
        params
        // params
      } = req.body;

      const exchange = new ccxt.bitmart({
        apiKey: process.env.apiKey,
        secret: process.env.secret,
        uid: process.env.uid,
        enableRateLimit: true,
    
    }) 
    if (exchange.has['fetchOrder']){
    try {
        const order = await exchange.fetchOrder (id, symbol); //Some exchanges allow you to specify optional parameters for your order. You can pass your optional parameters and override your query with an associative array using the params argument to your unified API call. All custom params are exchange-specific, of course, and aren’t interchangeable, do not expect those custom params for one exchange to work with another exchange.
        console.log (order);
        res.json(order);       
     
   } catch  (e) {
       if (e instanceof ccxt.NetworkError) {
           console.log (exchange.id, 'fetchOrder failed due to a network error:', e.message)
           res.json({
            NetworkError:e.message,
            });
       } else if (e instanceof ccxt.ExchangeError) {
           console.log (exchange.id, 'fetchOrder failed due to exchange error:', e.message)
           res.json({
            Exchange_Error:e.message,
            });
       } else {
           console.log (exchange.id, 'fetchOrder failed with:', e.message)
           res.json({Error: e.message});
           
       }
   }
} else {
    throw new Error (exchange.id + ' does not have the fetchOrder  method')
}
},

//-----------------------------------------------------------FETCH ORDERS-----------------------------------------------------------------------------
   
fetchOrders: async (req,res) => {      // fetches a list of open orders.
    const {
        // ExchangeId,
        symbol,
        since,
        limit
        // params
      } = req.body;

      const exchange = new ccxt.bitmart({
        apiKey: process.env.apiKey,
        secret: process.env.secret,
        uid: process.env.uid,
        enableRateLimit: true,
    
    }) 
    if (exchange.has['fetchOrders']){
    try {
        const order = await exchange.fetchOrders(symbol, since, limit); //Some exchanges allow you to specify optional parameters for your order. You can pass your optional parameters and override your query with an associative array using the params argument to your unified API call. All custom params are exchange-specific, of course, and aren’t interchangeable, do not expect those custom params for one exchange to work with another exchange.
        console.log (order);
        res.json(order);       
     
   } catch  (e) {
       if (e instanceof ccxt.NetworkError) {
           console.log (exchange.id, 'fetchOrders failed due to a network error:', e.message)
           res.json({
            NetworkError:e.message,
            });
       } else if (e instanceof ccxt.ExchangeError) {
           console.log (exchange.id, 'fetchOrders failed due to exchange error:', e.message)
           res.json({
            Exchange_Error:e.message,
            });
       } else {
           console.log (exchange.id, 'fetchOrders failed with:', e.message)
           res.json({Error: e.message});
           
       }
   }
} else {
    throw new Error (exchange.id + ' does not have the fetchOrders  method')
}
},

}