const ccxt = require('ccxt');

module.exports = {
//--------------------------------------------------FETCH DEPOSIT ADDRESS---------------------------------------------------------------------------    
    fetchDepositAddress: async (req,res) => {
        const {
            code,
          } = req.body;
    
          const exchange = new ccxt.bitmart({
            apiKey: process.env.apiKey,
            secret: process.env.secret,
            uid: process.env.uid,
        })
        // exchange.has['createDepositAddress']
        // exchange.has['fetchDepositAddress'];
    
        try {
            const order = await exchange.fetchDepositAddress (code); //In order to deposit funds to an exchange you must get an address from the exchange for the currency you want to deposit there. Most of exchanges will create and manage those addresses for the user. 
            console.log (order);
            res.json(order);       
         
       } catch  (e) {
           if (e instanceof ccxt.NetworkError) {
               console.log (exchange.id, 'fetchDepositAddress failed due to a network error:', e.message)
               res.json({
                NetworkError:e.message,
                });
           } else if (e instanceof ccxt.ExchangeError) {
               console.log (exchange.id, 'fetchDepositAddress failed due to exchange error:', e.message)
               res.json({
                Exchange_Error:e.message,
                });
           } else {
               console.log (exchange.id, 'fetchDepositAddress failed with:', e.message)
               res.json(e.message);
               
           }
       }
    },
//--------------------------------------------------CREATE DEPOSIT ADDRESS---------------------------------------------------------------------------    

createDepositAddress:async (req,res) => {          // Not supported for Bitmart
        const {
            EXchangeId,
            code,
            params,
          } = req.body;
    
          const exchange = new ccxt[EXchangeId]({
            apiKey: process.env.apiKey,
            secret: process.env.secret,
            uid: process.env.uid,
        })
        if (exchange.has['createDepositAddress']) {
        try {
            const order = await exchange.createDepositAddress (code, params); 
            console.log (order);
            res.json(order);       
         
       } catch  (e) {
           if (e instanceof ccxt.NetworkError) {
               console.log (exchange.id, 'fetchDeposits failed due to a network error:', e.message)
               res.json({
                NetworkError:e.message,
                });
           } else if (e instanceof ccxt.ExchangeError) {
               console.log (exchange.id, 'fetchDeposits failed due to exchange error:', e.message)
               res.json({
                Exchange_Error:e.message,
                });
           } else {
               console.log (exchange.id, 'fetchDeposits failed with:', e.message)
               res.json(e.message);
               
           }
       }
    } else {
       res.json("This exchange does not have the createDepositAddress method");
        
    }
    },

//--------------------------------------------------WITHDRAW---------------------------------------------------------------------------    
withdraw: async (req,res) => {
    const {
        code,
        amount,
        address,
        tag,
      } = req.body;

      const exchange = new ccxt.bitmart({
        apiKey: process.env.apiKey,
        secret: process.env.secret,
        uid: process.env.uid,
    })

    try {
        const order = await exchange.withdraw (code, amount, address, tag); 
        console.log (order);
        res.json(order);       
     
   } catch  (e) {
       if (e instanceof ccxt.NetworkError) {
           console.log (exchange.id, 'withdraw failed due to a network error:', e.message)
           res.json({
            NetworkError:e.message,
            });
       } else if (e instanceof ccxt.ExchangeError) {
           console.log (exchange.id, 'withdraw failed due to exchange error:', e.message)
           res.json({
            Exchange_Error:e.message,
            });
       } else {
           console.log (exchange.id, 'withdraw failed with:', e.message)
           res.json(e.message);
           
       }
   }
},
//--------------------------------------------------FETCH DEPOSITS---------------------------------------------------------------------------    

fetchDeposits:async (req,res) => {
    const {
        code,
        since,
        limit,
        params,
      } = req.body;

      const exchange = new ccxt.bitmart({
        apiKey: process.env.apiKey,
        secret: process.env.secret,
        uid: process.env.uid,
    })
    if (exchange.has['fetchDeposits']) {
    try {
        const order = await exchange.fetchDeposits (code, since, limit, params); 
        console.log (order);
        res.json(order);       
     
   } catch  (e) {
       if (e instanceof ccxt.NetworkError) {
           console.log (exchange.id, 'fetchDeposits failed due to a network error:', e.message)
           res.json({
            NetworkError:e.message,
            });
       } else if (e instanceof ccxt.ExchangeError) {
           console.log (exchange.id, 'fetchDeposits failed due to exchange error:', e.message)
           res.json({
            Exchange_Error:e.message,
            });
       } else {
           console.log (exchange.id, 'fetchDeposits failed with:', e.message)
           res.json(e.message);
           
       }
   }
} else {
    throw new Error (exchange.id + ' does not have the fetchDeposits method')
}
},

//--------------------------------------------------WITHDRAW---------------------------------------------------------------------------    
withdraw: async (req,res) => {
    const {
        code,
        amount,
        address,
        tag,
      } = req.body;

      const exchange = new ccxt.bitmart({
        apiKey: process.env.apiKey,
        secret: process.env.secret,
        uid: process.env.uid,
    })

    try {
        const order = await exchange.withdraw (code, amount, address, tag); 
        console.log (order);
        res.json(order);       
     
   } catch  (e) {
       if (e instanceof ccxt.NetworkError) {
           console.log (exchange.id, 'withdraw failed due to a network error:', e.message)
           res.json({
            NetworkError:e.message,
            });
       } else if (e instanceof ccxt.ExchangeError) {
           console.log (exchange.id, 'withdraw failed due to exchange error:', e.message)
           res.json({
            Exchange_Error:e.message,
            });
       } else {
           console.log (exchange.id, 'withdraw failed with:', e.message)
           res.json(e.message);
           
       }
   }
},
//--------------------------------------------------FETCH WITHDRAWLS---------------------------------------------------------------------------    

fetchWithdrawals:async (req,res) => {
    const {
        code,
        since,
        limit,
        params,
      } = req.body;

      const exchange = new ccxt.bitmart({
        apiKey: process.env.apiKey,
        secret: process.env.secret,
        uid: process.env.uid,
    })
    if (exchange.has['fetchWithdrawals']) {
    try {
        const order = await exchange.fetchWithdrawals (code, since, limit, params); 
        console.log (order);
        res.json(order);       
     
   } catch  (e) {
       if (e instanceof ccxt.NetworkError) {
           console.log (exchange.id, 'fetchWithdrawals failed due to a network error:', e.message)
           res.json({
            NetworkError:e.message,
            });
       } else if (e instanceof ccxt.ExchangeError) {
           console.log (exchange.id, 'fetchWithdrawals failed due to exchange error:', e.message)
           res.json({
            Exchange_Error:e.message,
            });
       } else {
           console.log (exchange.id, 'fetchWithdrawals failed with:', e.message)
           res.json(e.message);
           
       }
   }
} else {
    throw new Error (exchange.id + ' does not have the fetchWithdrawals method')
}
},

//--------------------------------------------------ALL TRANSACTIONS---------------------------------------------------------------------------    

fetchTransactions:async (req,res) => {           // Not supported for Bitmart
    const {
        code,
        since,
        limit,
        params,
      } = req.body;

      const exchange = new ccxt.bitmart({
        apiKey: process.env.apiKey,
        secret: process.env.secret,
        uid: process.env.uid,
    })
    if (exchange.has['fetchTransactions']) {
    try {
        const order = await exchange.fetchTransactions (code, since, limit, params); 
        console.log (order);
        res.json(order);       
     
   } catch  (e) {
       if (e instanceof ccxt.NetworkError) {
           console.log (exchange.id, 'fetchTransactions failed due to a network error:', e.message)
           res.json({
            NetworkError:e.message,
            });
       } else if (e instanceof ccxt.ExchangeError) {
           console.log (exchange.id, 'fetchTransactions failed due to exchange error:', e.message)
           res.json({
            Exchange_Error:e.message,
            });
       } else {
           console.log (exchange.id, 'fetchTransactions failed with:', e.message)
           res.json(e.message);
           
       }
   }
} else {
    throw new Error (exchange.id + ' does not have the fetchTransactions method')
}
},

//-------------------------------------------------FETCH FUNDING & TRADING FEES--------------------------------------------------------------------------------------
//fetchFees will automatically call both fetchFundingFees and fetchTradingFees to get all the fee information.     

fetchFees:async (req,res) => {          
    const {
        params,
      } = req.body;

      const exchange = new ccxt.bitmart({
        apiKey: process.env.apiKey,
        secret: process.env.secret,
        uid: process.env.uid,
    })
    if (exchange.has['fetchFees']) {
    try {
        const order = await exchange.fetchFees (); 
        console.log (order);
        res.json(order);       
     
   } catch  (e) {
       if (e instanceof ccxt.NetworkError) {
           console.log (exchange.id, 'fetchFees failed due to a network error:', e.message)
           res.json({
            NetworkError:e.message,
            });
       } else if (e instanceof ccxt.ExchangeError) {
           console.log (exchange.id, 'fetchFees failed due to exchange error:', e.message)
           res.json({
            Exchange_Error:e.message,
            });
       } else {
           console.log (exchange.id, 'fetchFees failed with:', e.message)
           res.json(e.message);
           
       }
   }
} else {
    throw new Error (exchange.id + ' does not have the fetchFees method')
}
},

//-------------------------------------------------FETCH TRADING FEES--------------------------------------------------------------------------------------
//Trading fees. Trading fee is the amount payable to the exchange, usually a percentage of volume traded (filled)).    

fetchTradingFees :async (req,res) => {          
    const {
        params,
      } = req.body;

      const exchange = new ccxt.bitmart({
        apiKey: process.env.apiKey,
        secret: process.env.secret,
        uid: process.env.uid,
    })
    if (exchange.has['fetchTradingFees']) {
    try {
        const order = await exchange.fetchTradingFees (params = {})
        ; 
        console.log (order);
        res.json(order);       
     
   } catch  (e) {
       if (e instanceof ccxt.NetworkError) {
           console.log (exchange.id, 'fetchTradingFees  failed due to a network error:', e.message)
           res.json({
            NetworkError:e.message,
            });
       } else if (e instanceof ccxt.ExchangeError) {
           console.log (exchange.id, 'fetchTradingFees  failed due to exchange error:', e.message)
           res.json({
            Exchange_Error:e.message,
            });
       } else {
           console.log (exchange.id, 'fetchTradingFees  failed with:', e.message)
           res.json(e.message);
           
       }
   }
} else {
    res.json( ' does not have the fetchTradingFees  method');
}
},

//-------------------------------------------------FETCH FUNDING FEES--------------------------------------------------------------------------------------
//Funding fees. The amount payable to the exchange upon depositing and withdrawing as well as the underlying crypto transaction fees (tx fees).    

fetchFundingFees :async (req,res) => {          
    const {
        params,
      } = req.body;

      const exchange = new ccxt.bitmart({
        apiKey: process.env.apiKey,
        secret: process.env.secret,
        uid: process.env.uid,
    })
    if (exchange.has['fetchFundingFees']) {
    try {
        const order = await exchange.fetchFundingFees(); 
        console.log (order);
        res.json(order);       
     
   } catch  (e) {
       if (e instanceof ccxt.NetworkError) {
           console.log (exchange.id, 'fetchFundingFees  failed due to a network error:', e.message)
           res.json({
            NetworkError:e.message,
            });
       } else if (e instanceof ccxt.ExchangeError) {
           console.log (exchange.id, 'fetchFundingFees  failed due to exchange error:', e.message)
           res.json({
            Exchange_Error:e.message,
            });
       } else {
           console.log (exchange.id, 'fetchFundingFees  failed with:', e.message)
           res.json(e.message);
           
       }
   }
} else {
    throw new Error (exchange.id + ' does not have the fetchFundingFees  method')
}
},

//-----------------------------------------------------------CALCULATE FEE-----------------------------------------------------------------------------
//The calculateFee method will return a unified fee structure with precalculated fees for an order with specified params.

calculateFee: async (req,res) => {
    const {
        // ExchangeId,
        symbol,
        type,
        side,
        amount,
        price,
        takerOrMaker,
        params
      } = req.body;
    
    const exchange = new ccxt.bitmart({
        apiKey: process.env.apiKey,
        secret: process.env.secret,
        uid: process.env.uid,
         
    });
    if (exchange.has['fetchFundingFees']) {
    try {
         const order = await exchange.calculateFee (symbol, type, side, amount, price, takerOrMaker, params = {})
         console.log (order);                             
         res.json(order);       
      
    } catch  (e) {
        if (e instanceof ccxt.NetworkError) {
            console.log (exchange.id, 'calculateFee failed due to a network error:', e.message)
            res.json({
                NetworkError:e.message,
                });
        } else if (e instanceof ccxt.ExchangeError) {
            console.log (exchange.id, 'calculateFee failed due to exchange error:', e.message)
            res.json({
                Exchange_Error:e.message,
                });
        } else {
            console.log (exchange.id, 'calculateFee failed with:', e.message)
            
        }
    }
} else {
    throw new Error (exchange.id + ' does not have the calculateFee  method')
}
    
},
//------------------------------------Accessing trading fee rates should be done via the .markets property-------------------------------
//The markets stored under the .markets property may contain additional fee related information:

calculateViaMarket: async (req,res) => {
    // const {
    //     // ExchangeId,
    //     symbol,
    //     type,
    //     side,
    //     amount,
    //     price,
    //     takerOrMaker,
    //     params
    //   } = req.body;
    
    const exchange = new ccxt.bitmart({
        apiKey: process.env.apiKey,
        secret: process.env.secret,
        uid: process.env.uid,
         
    });
    try {
         const order = await exchange.markets['ETH/BTC']['taker'] // taker fee rate for ETH/BTC
         const order2 = await exchange.markets['BTC/USDT']['maker'] // maker fee rate for BTC/USDT

         console.log (order);   
         console.log(order2);                          
         res.json({order,order2});       
      
    } catch  (e) {
        if (e instanceof ccxt.NetworkError) {
            console.log (exchange.id, 'calculateViaMarket failed due to a network error:', e.message)
            res.json({
                NetworkError:e.message,
                });
        } else if (e instanceof ccxt.ExchangeError) {
            console.log (exchange.id, 'calculateViaMarket failed due to exchange error:', e.message)
            res.json({
                Exchange_Error:e.message,
                });
        } else {
            console.log (exchange.id, 'calculateViaMarket failed with:', e.message)
            
        }
    }
    
},

//---------------------------Accessing funding fee rates should be done via the .currencies property---------------------------------------------------------------------------

//Some exchanges have an endpoint for fetching the funding fee schedule, this is mapped to the unified method fetchFundingFees:
fundingFeeViaCurrencies: async (req,res) => {
    // const {
    //     // ExchangeId,
    //     symbol,
    //     type,Some exchanges have an endpoint for fetching the funding fee schedule, this is mapped to the unified method fetchFundingFees:
    //     side,
    //     amount,
    //     price,
    //     takerOrMaker,
    //     params
    //   } = req.body;
    
    const exchange = new ccxt.bitmart({
        apiKey: process.env.apiKey,
        secret: process.env.secret,
        uid: process.env.uid,
         
    });
    try {
         const order = await exchange.currencies['ETH']['fee'] // tx/withdrawal fee rate for ETH
         console.log (order);                             
         res.json(order);       
      
    } catch  (e) {
        if (e instanceof ccxt.NetworkError) {
            console.log (exchange.id, 'fundingFeeViaCurrencies failed due to a network error:', e.message)
            res.json({
                NetworkError:e.message,
                });
        } else if (e instanceof ccxt.ExchangeError) {
            console.log (exchange.id, 'fundingFeeViaCurrencies failed due to exchange error:', e.message)
            res.json({
                Exchange_Error:e.message,
                });
        } else {
            console.log (exchange.id, 'fundingFeeViaCurrencies failed with:', e.message)
            
        }
    }
    
},

//----------------------------------------------------------FETCH LEDGER-----------------------------------------------------------------------------
//The calculateFee method will return a unified fee structure with precalculated fees for an order with specified params.

fetchLedger: async (req,res) => {
    const {
        // ExchangeId,
        code,
        since,
        limit,
        params
      } = req.body;
    
    const exchange = new ccxt.bitmart({
        apiKey: process.env.apiKey,
        secret: process.env.secret,
        uid: process.env.uid,
         
    });
    if (exchange.has['fetchLedger']) {
    try {
         const order = await exchange.fetchLedger (code, since, limit, params = {})
         console.log (order);                             
         res.json(order);       
      
    } catch  (e) {
        if (e instanceof ccxt.NetworkError) {
            console.log (exchange.id, 'fetchLedger failed due to a network error:', e.message)
            res.json({
                NetworkError:e.message,
                });
        } else if (e instanceof ccxt.ExchangeError) {
            console.log (exchange.id, 'fetchLedger failed due to exchange error:', e.message)
            res.json({
                Exchange_Error:e.message,
                });
        } else {
            console.log (exchange.id, 'fetchLedger failed with:', e.message)
            
        }
    }
} else {
    throw new Error (exchange.id + ' does not have the fetchLedger  method')
}
    
},


}