const ccxt = require('ccxt');

module.exports = {
//---------------------------------------FETCH ALL EXCHANGES--------------------------------------------------------------------
    getAllExchanges : async (req,res) => {
        try {
            const exchange = await ccxt.exchanges;
            res.json(exchange);
        } catch (err) {
            res.json({ message: err});
        }
    },
//---------------------------------------FETCH ALL EXCHANGES--------------------------------------------------------------------
    
    getAllMarkets : async (req, res) => {
        try {    
                const exchange = new ccxt[req.params.key];
                const markets = await exchange.loadMarkets(); 
                console.log(markets);
                res.json(markets);
    
        } catch (err) {
            res.json({ message: err});
        }
    },

    fetchTicker: async (req,res) => {
        const {
            ExchangeId,
            market
          } = req.body;
          const exchange = new ccxt[ExchangeId];
    
      
        try {
               
            const ticker = await exchange.fetchTicker(market);
            console.log(ticker);
            res.json(ticker);
                       
          
        } catch  (e) {
            // if the exception is thrown, it is "caught" and can be handled here
            // the handling reaction depends on the type of the exception
            // and on the purpose or business logic of your application
            if (e instanceof ccxt.NetworkError) {
                console.log (exchange.id, 'fetchTicker failed due to a network error:', e.message)
                // retry or whatever
                // ...
            } else if (e instanceof ccxt.ExchangeError) {
                console.log (exchange.id, 'fetchTicker failed due to exchange error:', e.message)
                // retry or whatever
                // ...
            } else {
                console.log (exchange.id, 'fetchTicker failed with:', e.message)
                // retry or whatever
                // ...
            }
        }
    },

    orderBook: async (req,res) => {
        const {
            ExchangeId,
            market
          } = req.body;
          const exchange = new ccxt[ExchangeId];
          
          if (exchange.has['fetchOrderBook']) {
      
        try {
               
            const orderbook = await exchange.fetchOrderBook(market);
            console.log(orderbook);
            res.json(orderbook);
                       
          
        } catch  (e) {
            if (e instanceof ccxt.NetworkError) {
                console.log (ExchangeId, 'fetchOrderBook failed due to a network error:', e.message)
                res.json({
                    NetworkError:e.message,
                    });
            } else if (e instanceof ccxt.ExchangeError) {
                console.log (ExchangeId, 'fetchOrderBook failed due to exchange error:', e.message)
                res.json({
                    Exchange_Error:e.message,
                    });
            } else {
                console.log (ExchangeId, 'fetchOrderBook failed with:', e.message)
                
            }
        }
    } else {
        throw new Error (ExchangeId + ' does not have the fetchOrderBook  method')
    }
    },

    ohlcv:  async (req,res) => {
        const {
            ExchangeId,
            market
          } = req.body;

          const exchange = new ccxt[ExchangeId];
          
          if (exchange.has['fetchOHLCV']) {

        try {
               
            const ohlcv = await exchange.fetchOHLCV(market);
            console.log(ohlcv);
            res.json(ohlcv);
                       
          
        } catch  (e) {
            if (e instanceof ccxt.NetworkError) {
                console.log (ExchangeId, 'ohlcv failed due to a network error:', e.message)
                res.json({
                    NetworkError:e.message,
                    });
            } else if (e instanceof ccxt.ExchangeError) {
                console.log (ExchangeId, 'ohlcv failed due to exchange error:', e.message)
                res.json({
                    Exchange_Error:e.message,
                    });
            } else {
                console.log (ExchangeId, 'ohlcv failed with:', e.message)
                
            }
        }
    } else {
        throw new Error (ExchangeId + ' does not have the ohlcv  method')
    }
    },
//----------------------------------------------------------FETCH TRADES-----------------------------------------------------------------------------
//You can call the unified fetchTrades / fetch_trades method to get the list of most recent trades for a particular symbol. The fetchTrades method is declared in the following way:

//async fetchTrades (symbol, since = undefined, limit = undefined, params = {})
//For example, if you want to print recent trades for all symbols one by one sequentially (mind the rateLimit!) you would do it like so:

fetchTrades: async (req,res) => {
    const {
        ExchangeId,
        symbol,
        since,
        limit,
        params
      } = req.body;
    
      const exchange = new ccxt[ExchangeId];
    if (exchange.has['fetchTrades']) {
    try {
         const order = await exchange.fetchTrades (symbol, since, limit, params)
         console.log (order);                             
         res.json(order);       
      
    } catch  (e) {
        if (e instanceof ccxt.NetworkError) {
            console.log (exchange.id, 'fetchTrades failed due to a network error:', e.message)
            res.json({
                NetworkError:e.message,
                });
        } else if (e instanceof ccxt.ExchangeError) {
            console.log (exchange.id, 'fetchTrades failed due to exchange error:', e.message)
            res.json({
                Exchange_Error:e.message,
                });
        } else {
            console.log (exchange.id, 'fetchTrades failed with:', e.message)
            
        }
    }
} else {
    throw new Error (exchange.id + ' does not have the fetchTrades  method')
}
    
},


}