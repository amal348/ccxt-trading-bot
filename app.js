const express = require('express');
//IMPORT
const app = express();
// const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
require('dotenv/config');


//MIDDLEWARES
app.use(cors());
app.use(bodyParser.json());


//IMPORT PUBLIC ROUTES
const PublicApiRoute = require('./routes/PublicApiRoute');
app.use('/', PublicApiRoute);


//IMPORT PRIVATE ROUTES
const PrivateApiController = require('./routes/PrivateApiRoute');
app.use('/', PrivateApiController);


//IMPORT PAYMENTS ROUTES
const paymentsApiController = require('./routes/paymentsRoute');
app.use('/', paymentsApiController);


//TRIAL ROUTE
// const orderbookRoute = require('./routes/orderbook');
// app.use('/orderbook', orderbookRoute);




//-------------------------------------------------------------------





//ROUTES
app.get('/', (req, res) => {
    res.send('We are on Home');
});



//Connect to DB
// mongoose.connect(process.env.DB_CONNECTION, () =>
// console.log('Connected to DB!')
// );
//HOW TO START LISTENING TO THE SERVER

app.listen(process.env.PORT);





