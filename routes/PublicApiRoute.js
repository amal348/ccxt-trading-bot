const express = require('express');
const router = express.Router();
const PublicApiController = require('../controllers/PublicApiController');


//GET ALL EXCHANGES
router.get('/exchanges', PublicApiController.getAllExchanges);
//GET ALL MARKETS BY SPECIFYING EXCHANGE ID
router.get('/markets/:key', PublicApiController.getAllMarkets);
//FETCH TICKER
router.get('/fetchticker', PublicApiController.fetchTicker);
//FETCH ORDER BOOK
router.post('/orderbook', PublicApiController.orderBook);
//FETCH OHLCV
router.post('/ohlcv', PublicApiController.ohlcv);
//FETCH TRADES
router.post('/fetchTrades', PublicApiController.fetchTrades);
module.exports = router;