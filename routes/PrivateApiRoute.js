const express = require('express');
const router = express.Router();
const PrivateApiController = require('../controllers/PrivateApiController');


//FETCH BALANCE
router.get('/fetchbalance', PrivateApiController.fetchBalance);
//FETCH USER TRADE
router.get('/fetchmytrade', PrivateApiController.fetchMyTrade);
//CREATE ORDER
router.post('/createorder', PrivateApiController.createOrder);
//CREATE MARKET BUY ORDER  
router.post('/createmarketbuyorder', PrivateApiController.createMarketBuyOrder);
//CREATE MARKET SELL ORDER  
router.post('/createmarketsellorder', PrivateApiController.createMarketSellOrder);
//CREATE LIMIT BUY ORDER  
router.post('/createlimitbuyorder', PrivateApiController.createLimitBuyOrder);
//CREATE LIMIT SELL ORDER  
router.post('/createlimitsellorder', PrivateApiController.createLimitSellOrder)
//CREATE ORDER WITH CLIENT ORDER ID AS PARAMETER
router.post('/createOrder_with_clientOrderId', PrivateApiController.createOrder_with_clientOrderId);
//CREATE ORDER WITH STOP LIMIT AS PARAMETER
router.post('/createOrder_with_stopLimit', PrivateApiController.createOrder_with_stopLimit);
//CANCEL A ORDER WITH ORDER ID
router.post('/cancelOrder', PrivateApiController.cancelOrder);
//FETCH OPEN ORDERS
router.post('/fetchOpenOrders', PrivateApiController.fetchOpenOrders);
//FETCH CLOSED ORDERS
router.post('/fetchClosedOrders', PrivateApiController.fetchClosedOrders);
//FETCH ORDER
router.post('/fetchOrder', PrivateApiController.fetchOrder);
//FETCH ORDERS
router.post('/fetchOrders', PrivateApiController.fetchOrders);
module.exports = router;
