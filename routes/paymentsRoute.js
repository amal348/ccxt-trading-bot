const express = require('express');
const router = express.Router();
const payments = require('../controllers/paymentsApiController');

//FETCH DEPOSIT ADDRESS
router.post('/fetchDepositAddress', payments.fetchDepositAddress);
//WITHDRAW
router.post('/withdraw', payments.withdraw);
//CREATE DEPOSIT ADDRESS
router.post('/createDepositAddress', payments.createDepositAddress);
//FETCH DEPOSITS
router.post('/fetchDeposits', payments.fetchDeposits);
//FETCH WITHDRAWALS
router.post('/fetchWithdrawals', payments.fetchWithdrawals);
//FETCH ALL TRANSACTIONS
router.post('/fetchTransactions', payments.fetchTransactions);
//FETCH FUNDINNG & TRADING FEES
router.post('/fetchFees', payments.fetchFees);
//FETCH TRADING FEES
router.post('/fetchTradingFees', payments.fetchTradingFees);
//FETCH FUNDING FEES
router.post('/fetchFundingFees', payments.fetchFundingFees);
//CALCULATE FEES
router.post('/calculateFee', payments.calculateFee);
//CALCULATE FEES VIA .MARKET PROPERTY
router.post('/calculateViaMarket', payments.calculateViaMarket);
//CALCULATE FUNDIN FEES VIA .CURRENCIES PROPERTY
router.post('/fundingFeeViaCurrencies', payments.fundingFeeViaCurrencies);
//FETCH FETCH LEDGER
router.post('/fetchLedger', payments.fetchLedger);

module.exports = router;
