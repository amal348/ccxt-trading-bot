
#   CCXT TRADING BOT API

The ccxt libraryis a collection of available crypto exchanges or exchange classes. Each class
implements the public and private API for a particular crypto exchange. All exchanges are
derived from the base Exchange class and share a set of common methods. To access a particular
exchange from ccxt libraryyou need to create an instance of corresponding exchange class.
Supported exchanges are updated frequently and new exchanges are added regularly and this is a cutom 
API for testing all the functionlities and methods. Here some where we using exchange id as bitmart and binance.



## Installation

Install Node Js and start with npm

```bash
  git clone https://gitlab.com/amal348/ccxt-trading-bot.git
  cd ccxt-trading-bot
  npm install 
  npm start

```
    
## API Testing

Import the following JSON link in postmon to test the public, private and other API's
```bash
  https://www.getpostman.com/collections/28170820247faf54cab1

```